const express  = require('express');
const socket  = require("socket.io");
const cors = require("cors");

const app = express();
app.use(express.static('public'));
const server = app.listen(4000, () => console.log("Listning at http://localhost:4000"));
const io = socket(server);
io.on('connection', soc => {
    console.log("connected on socket with ID "+ soc.id);
    soc.on('chat-message', function(data){
        console.log("chat-message", data);
        io.sockets.emit('chat-message', data);
    });
    soc.on('whoTyping', function(username){
        console.log("typing", username);
        soc.broadcast.emit('whoTyping', username);
    });
});