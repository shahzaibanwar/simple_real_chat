const socket = io();
let username = document.getElementById('txt-username');
let message = document.getElementById('txt-message');
let btn = document.getElementById('btn');
const messageContainer = document.getElementById('msg_container');
const feedback = document.getElementById('feedback');

btn.addEventListener('click', function(){
    socket.emit('chat-message', {
        username: username.value,
        message:message.value,
    });
    username.value = '';
    message.value = '';
});
message.addEventListener('keyup', function(){
    console.log("keyup");
    socket.emit('whoTyping', username.value);
});

socket.on('chat-message', function(data){
    console.log("message for append", data);
    feedback.innerHTML = '';
    messageContainer.innerHTML += `
    <div class="d-flex justify-content-end mb-4">
    <div class="msg_cotainer_send">
        ${data.message}
        <span class="msg_time_send">From, ${data.username}</span>
    </div>
    <div class="img_cont_msg">
        <img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg" />
        </div>
</div>
`;
});
socket.on('whoTyping', function(username){
    console.log("name", username);
    feedback.innerHTML = `${username} is typing...`;
    
});